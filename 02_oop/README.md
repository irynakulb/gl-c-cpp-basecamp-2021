# C/C++, OOP, STL

Language/libraries: C/C++, STL, usage of additional libraries or 3rd party code is prohibited.
For threading should be used platform-dependent API (unless specified otherwise by mentor).
For UI might be used winAPI, Qt etc.
The application should be written with using OOP.
